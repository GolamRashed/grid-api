#!/usr/bin/env python

""" Run script """

from grid.grid import app

if __name__ == '__main__':
    app.run(host=app.config["FLASK_HOST"], port=app.config["FLASK_PORT"])
